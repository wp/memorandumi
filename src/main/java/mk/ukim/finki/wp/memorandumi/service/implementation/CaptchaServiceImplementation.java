package mk.ukim.finki.wp.memorandumi.service.implementation;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.HashMap;
import java.util.Map;

@Service
public class CaptchaServiceImplementation {
    @Value("${recaptcha.secret.key}")
    private String recaptchaSecretKey;

    private static String RECAPTCHA_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify";

    public boolean verify(String recaptchaResponse) {
        RestTemplate restTemplate = new RestTemplate();
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("secret", recaptchaSecretKey);
        body.add("response", recaptchaResponse);

        Map<String, Object> response = restTemplate.postForObject(RECAPTCHA_VERIFY_URL, body, Map.class);
        return (Boolean)response.get("success");
    }
}
