package mk.ukim.finki.wp.memorandumi.service.implementation;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidCompanyException;
import mk.ukim.finki.wp.memorandumi.repository.CompanyRepository;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanyService;
import mk.ukim.finki.wp.memorandumi.service.interfaces.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CompanyServiceImplementation implements CompanyService {

    private final CompanyRepository companyRepository;

    public CompanyServiceImplementation(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    @Autowired
    private EmailService emailService;

    @Override
    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    @Override
    public Company findById(String id) {
        return companyRepository.findById(id).orElseThrow(InvalidCompanyException::new);
    }

    @Override
    public void save(String id, String name, String phone, String email, String companyDescription, String websiteUrl, MemorandumPackage memorandumPackage, byte[] logoImage, byte[] banner, boolean active) {
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setPhone(phone);
        company.setEmail(email);
        company.setCompanyDescription(companyDescription);
        company.setWebsiteUrl(websiteUrl);
        company.setMemorandumPackage(memorandumPackage);
        company.setLogoImage(logoImage);
        company.setBanner(banner);
        company.setActive(active);
        this.companyRepository.save(company);
    }

    @Override
    public void saveWithToken(String id, String name, String phone, String email, String companyDescription, String websiteUrl,
                              MemorandumPackage memorandumPackage, byte[] logoimg, byte[] bannerimg, boolean active, String token) {
        Company company = new Company();
        company.setId(id);
        company.setName(name);
        company.setPhone(phone);
        company.setEmail(email);
        company.setCompanyDescription(companyDescription);
        company.setWebsiteUrl(websiteUrl);
        company.setMemorandumPackage(memorandumPackage);
        company.setLogoImage(logoimg);
        company.setBanner(bannerimg);
        company.setActive(active);
        company.setToken(token);

        companyRepository.save(company);

        String url = "http://localhost:8080/company/verify?token=" + token;
        String message = "Please verify your email by clicking the following link: " + url;
        emailService.sendEmail(email, "Verify your email", message);
    }

    public void generateAndSendToken(String companyId) {
        Company company = this.findById(companyId);
        company.setToken(UUID.randomUUID().toString());
        companyRepository.save(company);
        String url = "http://localhost:8080/company/verify?token=" + company.getToken();
        String message = "Please verify your email by clicking the following link: " + url;
        emailService.sendEmail(company.getEmail(), "Verify your email", message);
    }

    @Override
    public Company findByToken(String token) {
        return companyRepository.findByToken(token);
    }


    @Override
    public void edit(String id, String name, String phone, String email, String companyDescription, String websiteUrl, MemorandumPackage memorandumPackage, byte[] logoImage, byte[] banner, boolean active) {
        Company company = this.companyRepository.findById(id).orElseThrow(InvalidCompanyException::new);
        company.setName(name);
        company.setPhone(phone);
        company.setEmail(email);
        company.setCompanyDescription(companyDescription);
        company.setWebsiteUrl(websiteUrl);
        company.setMemorandumPackage(memorandumPackage);
        company.setLogoImage(logoImage);
        company.setBanner(banner);
        company.setActive(active);
        this.companyRepository.save(company);
    }

    @Override
    public void delete(String companyId) {
        this.companyRepository.deleteById(companyId);
    }

    @Override
    public Page<Company> list(Specification<Company> spec, int page, int size) {
        return companyRepository.findAll(spec, PageRequest.of(page - 1, size));
    }

    @Override
    public List<Company> filterCompaniesByName(String text) {
        return this.companyRepository.findAllByName(text);
    }

    @Override
    public void save(Company company) {
        this.companyRepository.save(company);
    }

}
