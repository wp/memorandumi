package mk.ukim.finki.wp.memorandumi.config;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.Professor;
import mk.ukim.finki.wp.memorandumi.model.User;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidProfessorException;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidUsernameException;
import mk.ukim.finki.wp.memorandumi.repository.CompanyRepository;
import mk.ukim.finki.wp.memorandumi.repository.ProfessorRepository;
import mk.ukim.finki.wp.memorandumi.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class FacultyUserDetailsService implements UserDetailsService {

    private final CompanyRepository companyRepository;

    @Value("${system.authentication.password}")
    String systemAuthenticationPassword;

    final UserRepository userRepository;

    final ProfessorRepository professorService;

    final PasswordEncoder passwordEncoder;

    public FacultyUserDetailsService(UserRepository userRepository, ProfessorRepository professorService, PasswordEncoder passwordEncoder,
                                     CompanyRepository companyRepository) {
        this.userRepository = userRepository;
        this.professorService = professorService;
        this.passwordEncoder = passwordEncoder;
        this.companyRepository = companyRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findById(username).orElse(null);
        if (user == null) {
            Company company = companyRepository.findById(username).orElseThrow(InvalidUsernameException::new);
            return new FacultyUserDetails(user, company);
        } else if (user.getRole().isProfessor()) {
            Professor professor = professorService.findById(username).orElseThrow(InvalidProfessorException::new);
            return new FacultyUserDetails(user, professor, passwordEncoder.encode(systemAuthenticationPassword));
        } else {
            return new FacultyUserDetails(user, passwordEncoder.encode(systemAuthenticationPassword));
        }
    }
}
