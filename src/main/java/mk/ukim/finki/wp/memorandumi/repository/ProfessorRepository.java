package mk.ukim.finki.wp.memorandumi.repository;

import mk.ukim.finki.wp.memorandumi.model.Professor;

public interface ProfessorRepository extends JpaSpecificationRepository<Professor, String> {

}
