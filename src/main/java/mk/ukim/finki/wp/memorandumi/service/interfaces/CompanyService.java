package mk.ukim.finki.wp.memorandumi.service.interfaces;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.CompanySubscription;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidCompanyException;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;
import java.util.Optional;

public interface CompanyService {

    List<Company> getAllCompanies();
    Company findById(String id) throws InvalidCompanyException;

    Company findByToken(String token);

    void save(String id, String name, String phone, String email, String description,String websiteUrl, MemorandumPackage memorandumPackage, byte[] logoImage,byte[] banner,boolean active);

    void saveWithToken(String id, String name, String phone, String email, String companyDescription, String websiteUrl,
                       MemorandumPackage memorandumPackage, byte[] logoimg, byte[] bannerimg, boolean active, String token);

    void edit(String id,String name, String phone, String email, String companyDescription,String websiteUrl, MemorandumPackage memorandumPackage, byte[] logoImage,byte[] banner,boolean active);

    void delete(String companyId);
    Page<Company> list(Specification<Company> spec, int page, int size);

    List<Company> filterCompaniesByName(String text);

    void save(Company company);

    void generateAndSendToken(String companyId);
}
