package mk.ukim.finki.wp.memorandumi.service.interfaces;

public interface EmailService {
    void sendEmail(String to, String subject, String text);
}
