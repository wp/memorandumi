package mk.ukim.finki.wp.memorandumi.repository;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.CompanySubscription;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import mk.ukim.finki.wp.memorandumi.model.MemorandumStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanySubscriptionRepository extends JpaSpecificationRepository<CompanySubscription, Long> {
    List<CompanySubscription> findAllByCompany(Company company);
    List<CompanySubscription> findAllByYear(Short year);
    List<CompanySubscription> findAllByMemorandumPackage(MemorandumPackage memorandumPackage);
    List<CompanySubscription> findAllByStatus(MemorandumStatus status);

}
