package mk.ukim.finki.wp.memorandumi.service.interfaces;

import mk.ukim.finki.wp.memorandumi.model.WebsitePosting;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidWebsitePostingException;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface WebsitePostingService {
    List<WebsitePosting> getAllWebsitePostings();

    WebsitePosting findById(Long id) throws InvalidWebsitePostingException;

    void save(String companyId, String title, String body, byte[] image);

    void edit(Long id, String companyId, String title, String body, byte[] image);

    void delete(Long websitePostingId);

    Page<WebsitePosting> list(Specification<WebsitePosting> spec, int page, int size);
}
