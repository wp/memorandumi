package mk.ukim.finki.wp.memorandumi.repository;

import mk.ukim.finki.wp.memorandumi.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaSpecificationRepository<User, String> {

}
