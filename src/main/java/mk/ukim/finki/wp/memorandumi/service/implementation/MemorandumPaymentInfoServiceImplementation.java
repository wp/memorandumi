package mk.ukim.finki.wp.memorandumi.service.implementation;

import mk.ukim.finki.wp.memorandumi.model.CompanySubscription;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPaymentInfo;
import mk.ukim.finki.wp.memorandumi.model.MemorandumStatus;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidMemorandumPaymentException;
import mk.ukim.finki.wp.memorandumi.repository.MemorandumPaymentInfoRepository;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanySubscriptionService;
import mk.ukim.finki.wp.memorandumi.service.interfaces.MemorandumPaymentInfoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class MemorandumPaymentInfoServiceImplementation implements MemorandumPaymentInfoService {
    private final MemorandumPaymentInfoRepository memorandumPaymentInfoRepository;
    private final CompanySubscriptionService companySubscriptionService;

    public MemorandumPaymentInfoServiceImplementation(MemorandumPaymentInfoRepository memorandumPaymentInfoRepository, CompanySubscriptionService companySubscriptionService) {
        this.memorandumPaymentInfoRepository = memorandumPaymentInfoRepository;
        this.companySubscriptionService = companySubscriptionService;
    }

    @Override
    public MemorandumPaymentInfo findById(Long id) {
        return memorandumPaymentInfoRepository.findById(id).orElseThrow(InvalidMemorandumPaymentException::new);
    }

    @Override
    public MemorandumPaymentInfo create(Long companySubscriptionId, LocalDate paymentDate, Double amountMkd, String note) {
        CompanySubscription companySubscription = companySubscriptionService.findById(companySubscriptionId);
        companySubscription.setStatus(MemorandumStatus.PAYED);
        MemorandumPaymentInfo memorandumPaymentInfo = new MemorandumPaymentInfo(companySubscription, paymentDate, amountMkd, note);
        return memorandumPaymentInfoRepository.save(memorandumPaymentInfo);
    }

    @Override
    public MemorandumPaymentInfo update(Long id, Long companySubscriptionId, LocalDate paymentDate, Double amountMkd, String note) {
        MemorandumPaymentInfo memorandumPaymentInfo = this.findById(id);
        CompanySubscription companySubscription = companySubscriptionService.findById(companySubscriptionId);
        memorandumPaymentInfo.setCompanySubscription(companySubscription);
        memorandumPaymentInfo.setPaymentDate(paymentDate);
        memorandumPaymentInfo.setAmountMkd(amountMkd);
        memorandumPaymentInfo.setNote(note);
        return memorandumPaymentInfoRepository.save(memorandumPaymentInfo);
    }

    @Override
    public MemorandumPaymentInfo delete(Long id) {
        MemorandumPaymentInfo memorandumPaymentInfo = this.findById(id);
        memorandumPaymentInfo.companySubscription.setStatus(MemorandumStatus.SIGNED);
        memorandumPaymentInfoRepository.delete(memorandumPaymentInfo);
        return memorandumPaymentInfo;
    }

    @Override
    public Page<MemorandumPaymentInfo> list(Specification<MemorandumPaymentInfo> spec, int page, int size) {
        return memorandumPaymentInfoRepository.findAll(spec, PageRequest.of(page - 1, size));
    }
}
