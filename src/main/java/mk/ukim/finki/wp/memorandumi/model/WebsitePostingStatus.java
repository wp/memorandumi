package mk.ukim.finki.wp.memorandumi.model;

public enum WebsitePostingStatus {
    REQUESTED, APPROVED, REJECTED, PUBLISHED
}
