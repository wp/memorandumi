package mk.ukim.finki.wp.memorandumi.model;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@NoArgsConstructor
public class MemorandumPaymentInfo {

    @Id
    @GeneratedValue
    public Long id;

    @ManyToOne
    public CompanySubscription companySubscription;

    public LocalDate paymentDate;

    public Double amountMkd;

    @Column(length = 1_000)
    public String note;

    public MemorandumPaymentInfo(CompanySubscription companySubscription, LocalDate paymentDate, Double amountMkd, String note) {
        this.companySubscription = companySubscription;
        this.paymentDate = paymentDate;
        this.amountMkd = amountMkd;
        this.note = note;
    }
}
