package mk.ukim.finki.wp.memorandumi.repository;

import mk.ukim.finki.wp.memorandumi.model.Room;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaSpecificationRepository<Room, String> {
}
