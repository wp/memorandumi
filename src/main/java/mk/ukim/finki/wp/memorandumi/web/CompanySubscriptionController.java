package mk.ukim.finki.wp.memorandumi.web;

import mk.ukim.finki.wp.memorandumi.model.CompanySubscription;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import mk.ukim.finki.wp.memorandumi.model.MemorandumStatus;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanyService;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanySubscriptionService;
import mk.ukim.finki.wp.memorandumi.service.interfaces.RoomService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import mk.ukim.finki.wp.memorandumi.service.specifications.FieldFilterSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import static mk.ukim.finki.wp.memorandumi.service.specifications.FieldFilterSpecification.*;
import static org.springframework.data.jpa.domain.Specification.where;

import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/admin/company-subscriptions")
public class CompanySubscriptionController {
    private final CompanyService companyService;
    private final CompanySubscriptionService companySubscriptionService;
    private final RoomService roomService;

    public CompanySubscriptionController(CompanyService companyService,
                                         CompanySubscriptionService companySubscriptionService,
                                         RoomService roomService) {
        this.companyService = companyService;
        this.companySubscriptionService = companySubscriptionService;
        this.roomService = roomService;
    }

    @GetMapping
    public String showList(@RequestParam (required = false) String companyId,
                           @RequestParam (required = false) Short year,
                           @RequestParam (required = false) MemorandumPackage memorandumPackage,
                           @RequestParam (required = false) MemorandumStatus status,
                           @RequestParam(defaultValue = "1") Integer pageNum,
                           @RequestParam(defaultValue = "20") Integer results,
                           Model model) {

        Specification<CompanySubscription> spec = where(filterContainsText(CompanySubscription.class, "company.id", companyId));

        spec = spec.and(filterEqualsV(CompanySubscription.class, "year", year))
                .and(filterEqualsV(CompanySubscription.class, "memorandumPackage", memorandumPackage))
                .and(filterEqualsV(CompanySubscription.class, "status", status));


        Page<CompanySubscription> companySubscriptionsPage = companySubscriptionService.list(spec, pageNum, results);

        model.addAttribute("companyIdSelected", companyId);
        model.addAttribute("yearSelected", year);
        model.addAttribute("memorandumPackageSelected", memorandumPackage);
        model.addAttribute("statusSelected", status);

        model.addAttribute("memorandumPackages", MemorandumPackage.values());
        model.addAttribute("statuses", MemorandumStatus.values());
        model.addAttribute("companies", companyService.getAllCompanies());
        model.addAttribute("rooms", roomService.listAllRooms());

        model.addAttribute("page", companySubscriptionsPage);

        return "company-subscriptions/listCompanySubscriptions";
    }

    @GetMapping("/add")
    public String showAdd(Model model)
    {
        model.addAttribute("memorandumPackages", MemorandumPackage.values());
        model.addAttribute("statuses", MemorandumStatus.values());
        model.addAttribute("companies", companyService.getAllCompanies());
        model.addAttribute("rooms", roomService.listAllRooms());
        return "company-subscriptions/formCompanySubscription";
    }

    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable Long id, Model model)
    {
        model.addAttribute("companySubscription", companySubscriptionService.findById(id));
        model.addAttribute("memorandumPackages", MemorandumPackage.values());
        model.addAttribute("statuses", MemorandumStatus.values());
        model.addAttribute("companies", companyService.getAllCompanies());
        model.addAttribute("rooms", roomService.listAllRooms());
        return "company-subscriptions/formCompanySubscription";
    }

    @PostMapping
    public String create(@RequestParam String companyId,
                         @RequestParam Short year,
                         @RequestParam MemorandumPackage memorandumPackage,
                         @RequestParam MemorandumStatus status,
                         @RequestParam String brandedRoomId,
                         @RequestParam (required = false) String note,
                         Model model)
    {
        if (brandedRoomId.equals("none")) {
            brandedRoomId = null;
        }
        CompanySubscription companySubscription = this.companySubscriptionService.create(companyId,year, memorandumPackage, status, brandedRoomId, note);
        return "redirect:/admin/company-subscriptions/details/" + companySubscription.getId();
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         @RequestParam String companyId,
                         @RequestParam Short year,
                         @RequestParam MemorandumPackage memorandumPackage,
                         @RequestParam MemorandumStatus status,
                         @RequestParam String brandedRoomId,
                         @RequestParam (required = false) String note,
                         Model model)
    {
        if (brandedRoomId.equals("none")) {
            brandedRoomId = null;
        }
        this.companySubscriptionService.update(id, companyId,year, memorandumPackage, status, brandedRoomId, note);
        return "redirect:/admin/company-subscriptions/details/" + id;
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id){
        this.companySubscriptionService.delete(id);
        return "redirect:/admin/company-subscriptions";
    }

    @GetMapping("/details/{id}")
    public String showDetails(@PathVariable Long id, Model model) {
        CompanySubscription companySubscription = companySubscriptionService.findById(id);
        model.addAttribute("companySubscription", companySubscription);
        return "company-subscriptions/detailsCompanySubscription";
    }

    @GetMapping("/upload-contract/{id}")
    public String showUploadContract(@PathVariable Long id, Model model)
    {
        model.addAttribute("companySubscriptionId", id);
        return "company-subscriptions/formContractUpload";
    }

    @PostMapping("/upload-contract/{id}")
    public String uploadContract(@PathVariable Long id,
                                 @RequestParam("file") MultipartFile file) {
        try {
            byte[] contract = file.getBytes();
            this.companySubscriptionService.uploadContract(id, contract);
            return "redirect:/admin/company-subscriptions/details/" + id;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/download-contract/{id}")
    public ResponseEntity<byte[]> downloadContract(@PathVariable Long id) {
        CompanySubscription companySubscription = companySubscriptionService.findById(id);

        byte[] contract = companySubscription.contract;
        String companyName = companySubscription.company.name;
        Short year = companySubscription.year;

        if (contract == null || contract.length == 0) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }

        String filename = "contract_" + year + "_" + companyName + ".pdf";

        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
        headers.add(HttpHeaders.CONTENT_TYPE, "application/pdf");

        return ResponseEntity.ok()
                .headers(headers)
                .body(contract);
    }

    @PostMapping("/remove-contract/{id}")
    public String removeContract(@PathVariable Long id){
        this.companySubscriptionService.removeContract(id);
        return "redirect:/admin/company-subscriptions/details/" + id;
    }

}
