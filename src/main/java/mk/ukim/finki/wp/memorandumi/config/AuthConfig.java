package mk.ukim.finki.wp.memorandumi.config;

import mk.ukim.finki.wp.memorandumi.model.AppRole;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.web.access.expression.WebExpressionAuthorizationManager;

public class AuthConfig {

    public HttpSecurity authorize(HttpSecurity http) throws Exception {
        return http
                .authorizeHttpRequests((requests) -> requests
                        .requestMatchers(HttpMethod.OPTIONS).permitAll()
                        .requestMatchers("/",
                                "/company/register",
                                "/company/verify",
                                "/company/verification-email-sent",
                                "/company/set-password",
                                "/company/login",
                                "/company/reset-password",
                                "/company/forgot-password").permitAll()
                        .requestMatchers("/admin/companies",
                                "/admin/companies/add",
                                "/admin/companies/edit/**",
                                "/admin/companies/update/**",
                                "/admin/companies/delete/**",
                                "/admin/companies/details/**",
                                "/admin/company-subscriptions",
                                "/admin/company-subscriptions/add",
                                "/admin/company-subscriptions/edit/**",
                                "/admin/company-subscriptions/update/**",
                                "/admin/company-subscriptions/delete/**",
                                "/admin/company-subscriptions/details/**",
                                "/admin/company-subscriptions/upload-contract/**",
                                "/admin/company-subscriptions/download-contract/**",
                                "/admin/company-subscriptions/remove-contract/**",
                                "/admin/website-postings",
                                "/admin/website-postings/add",
                                "/admin/website-postings/edit/**",
                                "/admin/website-postings/update/**",
                                "/admin/website-postings/delete/**",
                                "/admin/website-postings/details/**",
                                "/admin/memorandum-payment-info",
                                "/admin/memorandum-payment-info/add",
                                "/admin/memorandum-payment-info/edit/**",
                                "/admin/memorandum-payment-info/update/**",
                                "/admin/memorandum-payment-info/delete/**",
                                "/admin/memorandum-payment-info/details/**").hasAnyRole(
                                AppRole.ADMIN.name()
                        )
                        .requestMatchers( "*.png", "*.jpg", "*.jpeg", "*.gif").permitAll()
                        .anyRequest().authenticated()
                )
                .logout(LogoutConfigurer::permitAll);
    }

}
