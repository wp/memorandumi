package mk.ukim.finki.wp.memorandumi.repository;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.WebsitePosting;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WebsitePostingRepository extends JpaSpecificationRepository<WebsitePosting, Long> {
    List<WebsitePosting> findAllByCompany(Company company);

    List<WebsitePosting> findAllByTitle(String title);

}