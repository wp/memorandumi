package mk.ukim.finki.wp.memorandumi.model;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class CompanySubscription {

    @Id
    @GeneratedValue
    public Long id;

    @ManyToOne
    public Company company;

    public Short year;

    @Enumerated(EnumType.STRING)
    public MemorandumPackage memorandumPackage;

    @Enumerated(EnumType.STRING)
    public MemorandumStatus status;

    public byte[] contract;

    // silver that pay extra, gold, supportedBy
    @ManyToOne
    public Room brandedRoom;

    @Column(length = 20_000)
    public String note;

    public CompanySubscription() {

    }

    public CompanySubscription(Company company, Short year, MemorandumPackage memorandumPackage, MemorandumStatus status, Room brandedRoom, String note) {
        this.company = company;
        this.year = year;
        this.memorandumPackage = memorandumPackage;
        this.status = status;
        this.brandedRoom = brandedRoom;
        this.note = note;
    }

}
