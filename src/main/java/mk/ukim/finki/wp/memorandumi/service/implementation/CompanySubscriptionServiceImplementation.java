package mk.ukim.finki.wp.memorandumi.service.implementation;

import mk.ukim.finki.wp.memorandumi.model.*;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidCompanyException;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidCompanySubscriptionIdException;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidRoomIdException;
import mk.ukim.finki.wp.memorandumi.repository.CompanyRepository;
import mk.ukim.finki.wp.memorandumi.repository.CompanySubscriptionRepository;
import mk.ukim.finki.wp.memorandumi.repository.RoomRepository;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanySubscriptionService;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

@Service
public class CompanySubscriptionServiceImplementation implements CompanySubscriptionService {

    private final CompanyRepository companyRepository;
    private final CompanySubscriptionRepository companySubscriptionRepository;
    private final RoomRepository roomRepository;

    public CompanySubscriptionServiceImplementation(CompanyRepository companyRepository, CompanySubscriptionRepository companySubscriptionRepository, RoomRepository roomRepository) {
        this.companyRepository = companyRepository;
        this.companySubscriptionRepository = companySubscriptionRepository;
        this.roomRepository = roomRepository;
    }

    @Override
    public List<CompanySubscription> listAllCompanySubscriptions() {
        return companySubscriptionRepository.findAll();
    }

    @Override
    public CompanySubscription findById(Long id) {
        return companySubscriptionRepository.findById(id).orElseThrow(InvalidCompanySubscriptionIdException::new);
    }

    @Override
    public CompanySubscription create(String companyId, Short year, MemorandumPackage memorandumPackage, MemorandumStatus status, String brandedRoomId, String note) {
        Company company = companyRepository.findById(companyId).orElseThrow(InvalidCompanyException::new);

        Room brandedRoom = null;
        if (brandedRoomId != null) {
            brandedRoom = roomRepository.findById(brandedRoomId).orElseThrow(InvalidRoomIdException::new);
        }

        CompanySubscription companySubscription = new CompanySubscription(company, year, memorandumPackage, status, brandedRoom, note);
        return companySubscriptionRepository.save(companySubscription);
    }

    @Override
    public CompanySubscription update(Long id, String companyId, Short year, MemorandumPackage memorandumPackage, MemorandumStatus status, String brandedRoomId, String note) {
        CompanySubscription companySubscription = this.findById(id);
        Company company = companyRepository.findById(companyId).orElseThrow(InvalidCompanyException::new);

        Room brandedRoom = null;
        if (brandedRoomId != null) {
            brandedRoom = roomRepository.findById(brandedRoomId).orElseThrow(InvalidRoomIdException::new);
        }

        companySubscription.setCompany(company);
        companySubscription.setYear(year);
        companySubscription.setMemorandumPackage(memorandumPackage);
        companySubscription.setStatus(status);
        companySubscription.setBrandedRoom(brandedRoom);
        companySubscription.setNote(note);

        return companySubscriptionRepository.save(companySubscription);
    }

    @Override
    public CompanySubscription delete(Long id) {
        CompanySubscription companySubscription = this.findById(id);
        companySubscriptionRepository.delete(companySubscription);
        return companySubscription;
    }

    @Override
    public CompanySubscription uploadContract(Long id, byte[] contract) {
        CompanySubscription companySubscription = this.findById(id);
        companySubscription.setContract(contract);
        companySubscription.setStatus(MemorandumStatus.SIGNED);
        companySubscription.company.setMemorandumPackage(companySubscription.getMemorandumPackage());

        return companySubscriptionRepository.save(companySubscription);
    }

    @Override
    public CompanySubscription removeContract(Long id) {
        CompanySubscription companySubscription = this.findById(id);
        companySubscription.setContract(null);
        companySubscription.setStatus(MemorandumStatus.INITIATED);
        return companySubscriptionRepository.save(companySubscription);
    }

    @Override
    public Page<CompanySubscription> list(Specification<CompanySubscription> spec, int page, int size) {
        return companySubscriptionRepository.findAll(spec, PageRequest.of(page - 1, size));
    }

}
