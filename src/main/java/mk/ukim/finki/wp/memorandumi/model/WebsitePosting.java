package mk.ukim.finki.wp.memorandumi.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class WebsitePosting {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Company company;

    public String title;

    @Column(length = 20_000)
    public String body;

    public byte[] image;


    @Enumerated(EnumType.STRING)
    public WebsitePostingStatus status;

    public WebsitePosting(Company company, String title, String body, byte[] image) {
        this.company = company;
        this.title = title;
        this.body = body;
        this.image = image;
    }

    public WebsitePosting() {

    }
}
