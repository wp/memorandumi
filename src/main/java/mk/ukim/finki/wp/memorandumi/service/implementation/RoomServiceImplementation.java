package mk.ukim.finki.wp.memorandumi.service.implementation;

import mk.ukim.finki.wp.memorandumi.model.Room;
import mk.ukim.finki.wp.memorandumi.repository.RoomRepository;
import mk.ukim.finki.wp.memorandumi.service.interfaces.RoomService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImplementation implements RoomService {
    private final RoomRepository roomRepository;

    public RoomServiceImplementation(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Override
    public List<Room> listAllRooms() {
        return roomRepository.findAll();
    }
}
