package mk.ukim.finki.wp.memorandumi.repository;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaSpecificationRepository<Company, String>{
    List<Company> findAllByName(String name);
    List<Company> findByNameContains(String text);
    List<Company> findAllByMemorandumPackage(MemorandumPackage memorandumPackage);
    List<Company> findAllByActive(Boolean active);
    Company findByToken(String token);
}
