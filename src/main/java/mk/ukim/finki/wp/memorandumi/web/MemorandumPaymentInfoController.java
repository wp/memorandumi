package mk.ukim.finki.wp.memorandumi.web;

import mk.ukim.finki.wp.memorandumi.model.MemorandumPaymentInfo;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanySubscriptionService;
import mk.ukim.finki.wp.memorandumi.service.interfaces.MemorandumPaymentInfoService;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

import static mk.ukim.finki.wp.memorandumi.service.specifications.FieldFilterSpecification.filterEqualsV;
import static org.springframework.data.jpa.domain.Specification.where;


@Controller
@RequestMapping("/admin/memorandum-payment-info")
public class MemorandumPaymentInfoController {
    private final MemorandumPaymentInfoService service;
    private final CompanySubscriptionService companySubscriptionService;

    public MemorandumPaymentInfoController(MemorandumPaymentInfoService service, CompanySubscriptionService companySubscriptionService) {
        this.service = service;
        this.companySubscriptionService = companySubscriptionService;
    }

    @GetMapping
    public String showList(@RequestParam(required = false) Long companySubscriptionId,
                           @RequestParam(required = false) LocalDate paymentDate,
                           @RequestParam(required = false) Double amountMkd,
                           @RequestParam(defaultValue = "1") Integer pageNum,
                           @RequestParam(defaultValue = "20") Integer results,
                           Model model) {

        Specification<MemorandumPaymentInfo> spec = where(filterEqualsV(MemorandumPaymentInfo.class, "companySubscription.id", companySubscriptionId));

        spec = spec.and(filterEqualsV(MemorandumPaymentInfo.class, "paymentDate", paymentDate))
                .and(filterEqualsV(MemorandumPaymentInfo.class, "amountMkd", amountMkd));

        Page<MemorandumPaymentInfo> memorandumPaymentInfoPage = service.list(spec, pageNum, results);

        model.addAttribute("companySubscriptions", companySubscriptionService.listAllCompanySubscriptions());
        model.addAttribute("page", memorandumPaymentInfoPage);

        model.addAttribute("companySubscriptionIdSelected", companySubscriptionId);
        model.addAttribute("paymentDateSelected", paymentDate);
        model.addAttribute("amountMkdSelected", amountMkd);

        return "memorandum-payment-info/listMemorandumPaymentInfo";
    }

    @GetMapping("/add")
    public String showAdd(Model model) {
        model.addAttribute("companySubscriptions", companySubscriptionService.listAllCompanySubscriptions());
        return "memorandum-payment-info/formMemorandumPaymentInfo";
    }

    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        model.addAttribute("paymentInfo", service.findById(id));
        model.addAttribute("companySubscriptions", companySubscriptionService.listAllCompanySubscriptions());
        return "memorandum-payment-info/formMemorandumPaymentInfo";
    }

    @GetMapping("/details/{id}")
    public String showDetails(@PathVariable Long id, Model model) {
        model.addAttribute("payment", service.findById(id));
        return "memorandum-payment-info/detailsMemorandumPaymentInfo";
    }


    @PostMapping
    public String create(@RequestParam Long companySubscriptionId,
                         @RequestParam LocalDate paymentDate,
                         @RequestParam Double amountMkd,
                         @RequestParam String note,
                         Model model) {
        try {
            this.service.create(companySubscriptionId, paymentDate, amountMkd, note);
            return "redirect:/admin/memorandum-payment-info";
        } catch (Exception e) {
            model.addAttribute("error", true);
            return "memorandum-payment-info/formMemorandumPaymentInfo";
        }
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         @RequestParam Long companySubscriptionId,
                         @RequestParam LocalDate paymentDate,
                         @RequestParam Double amountMkd,
                         @RequestParam String note,
                         Model model) {
        try {
            this.service.update(id, companySubscriptionId, paymentDate, amountMkd, note);
            return "redirect:/admin/memorandum-payment-info";
        } catch (Exception e) {
            model.addAttribute("error", true);
            return "memorandum-payment-info/formMemorandumPaymentInfo";
        }
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        this.service.delete(id);
        return "redirect:/admin/memorandum-payment-info";
    }
}
