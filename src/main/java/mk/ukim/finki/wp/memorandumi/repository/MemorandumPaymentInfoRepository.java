package mk.ukim.finki.wp.memorandumi.repository;

import mk.ukim.finki.wp.memorandumi.model.CompanySubscription;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPaymentInfo;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MemorandumPaymentInfoRepository extends JpaSpecificationRepository<MemorandumPaymentInfo, Long> {
    List<MemorandumPaymentInfo> findAllByCompanySubscription(CompanySubscription companySubscription);

    List<MemorandumPaymentInfo> findAllByPaymentDate(LocalDate date);

    List<MemorandumPaymentInfo> findAllByAmountMkd(Double amountMkd);

    List<MemorandumPaymentInfo> findAllByNote(String note);

}
