package mk.ukim.finki.wp.memorandumi.web;

import mk.ukim.finki.wp.memorandumi.model.WebsitePosting;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanyService;
import mk.ukim.finki.wp.memorandumi.service.interfaces.WebsitePostingService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.data.domain.Page;

import java.util.Base64;

import static org.springframework.data.jpa.domain.Specification.where;


import static mk.ukim.finki.wp.memorandumi.service.specifications.FieldFilterSpecification.filterContainsText;

@Controller
@RequestMapping("/admin/website-postings")
public class WebsitePostingController {

    private final WebsitePostingService websitePostingService;
    private final CompanyService companyService;


    public WebsitePostingController(WebsitePostingService websitePostingService, CompanyService companyService) {
        this.websitePostingService = websitePostingService;
        this.companyService = companyService;
    }

    @GetMapping
    public String showList(@RequestParam(required = false) String companyId,
                           @RequestParam(required = false) String title,
                           @RequestParam(defaultValue = "1") Integer pageNum,
                           @RequestParam(defaultValue = "20") Integer results,
                           Model model) {


        Specification<WebsitePosting> spec = where(filterContainsText(WebsitePosting.class, "company.id", companyId)).
                and(filterContainsText(WebsitePosting.class, "title", title));

        Page<WebsitePosting> websitePostingsPage = websitePostingService.list(spec, pageNum, results);

        model.addAttribute("companies", companyService.getAllCompanies());

        model.addAttribute("companyIdSelected", companyId);
        model.addAttribute("titleSelected", title);

        model.addAttribute("page", websitePostingsPage);
        return "website-postings/listWebsitePostings";
    }

    @GetMapping("/add")
    public String showAdd(Model model) {
        model.addAttribute("companies", companyService.getAllCompanies());
        return "website-postings/formWebsitePosting";
    }

    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable Long id, Model model) {
        WebsitePosting posting = websitePostingService.findById(id);
        model.addAttribute("websitePosting", posting);
        model.addAttribute("companies", companyService.getAllCompanies());
        String imageData = Base64.getEncoder().encodeToString(posting.getImage());
        model.addAttribute("imageData", imageData);
        return "website-postings/formWebsitePosting";
    }

    @PostMapping
    public String create(
            @RequestParam String companyId,
            @RequestParam String title,
            @RequestParam String body,
            @RequestParam MultipartFile image,
            Model model) {
        try {
            byte[] img = image.getBytes();
            this.websitePostingService.save(companyId, title, body, img);
            return "redirect:/admin/website-postings";
        } catch (Exception e) {
            model.addAttribute("error", true);
            return "website-postings/formWebsitePosting";
        }
    }

    @PostMapping("/update/{id}")
    public String update(@PathVariable Long id,
                         @RequestParam String companyId,
                         @RequestParam String title,
                         @RequestParam String body,
                         @RequestParam(required = false) MultipartFile image,
                         Model model) {
        try {
            WebsitePosting existingPosting = this.websitePostingService.findById(id);
            byte[] img = existingPosting.getImage();
            if (image != null && !image.isEmpty()) {
                img = image.getBytes();
            }
            this.websitePostingService.edit(id, companyId, title, body, img);
            return "redirect:/admin/website-postings";
        } catch (Exception e) {
            model.addAttribute("error", true);
            return "website-postings/formWebsitePosting";
        }
    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        this.websitePostingService.delete(id);
        return "redirect:/admin/website-postings";
    }

    @GetMapping("/details/{id}")
    public String showDetails(@PathVariable Long id, Model model) {
        WebsitePosting websitePosting = websitePostingService.findById(id);
        model.addAttribute("websitePosting", websitePosting);

        String imageData = Base64.getEncoder().encodeToString(websitePosting.getImage());
        model.addAttribute("imageData", imageData);

        return "website-postings/detailsWebsitePosting";
    }


}
