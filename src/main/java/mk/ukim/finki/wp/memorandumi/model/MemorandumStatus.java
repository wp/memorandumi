package mk.ukim.finki.wp.memorandumi.model;

public enum MemorandumStatus {
    INITIATED,
    SIGNED,
    PAYED
}
