package mk.ukim.finki.wp.memorandumi.service.interfaces;

import mk.ukim.finki.wp.memorandumi.model.MemorandumPaymentInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public interface MemorandumPaymentInfoService {
    MemorandumPaymentInfo findById(Long id);

    MemorandumPaymentInfo create(Long companySubscriptionId, LocalDate paymentDate, Double amountMkd, String note);

    MemorandumPaymentInfo update(Long id, Long companySubscriptionId, LocalDate paymentDate, Double amountMkd, String note);

    MemorandumPaymentInfo delete(Long id);

    Page<MemorandumPaymentInfo> list(Specification<MemorandumPaymentInfo> spec, int page, int size);
}
