package mk.ukim.finki.wp.memorandumi.web;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import mk.ukim.finki.wp.memorandumi.service.implementation.CaptchaServiceImplementation;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanyService;
import mk.ukim.finki.wp.memorandumi.service.interfaces.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Controller
public class CompanyRegistrationController {

    @Autowired
    private CompanyService companyService;
    @Autowired
    private CaptchaServiceImplementation captchaService;

    @Autowired
    private PasswordEncoder passwordEncoder;
    private final EmailService emailService;

    public CompanyRegistrationController(EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping("/company/register")
    public String registerCompanyForm(Model model) {
        model.addAttribute("memorandumPackages", MemorandumPackage.values());
        return "companies/registerCompany";
    }

    @PostMapping("/company/register")
    public String registerCompany(@RequestParam String id,
                                  @RequestParam String name,
                                  @RequestParam String phone,
                                  @RequestParam String email,
                                  @RequestParam String companyDescription,
                                  @RequestParam String websiteUrl,
                                  @RequestParam MemorandumPackage memorandumPackage,
                                  @RequestParam MultipartFile logoImage,
                                  @RequestParam MultipartFile banner,
                                  @RequestParam("g-recaptcha-response") String recaptchaResponse,
                                  Model model) {
        boolean isCaptchaValid = captchaService.verify(recaptchaResponse);
        if (!isCaptchaValid) {
            model.addAttribute("error", "Invalid reCAPTCHA.");
            return "companies/registerCompany";
        }

        try {
            byte[] logoimg = logoImage.getBytes();
            byte[] bannerimg = banner.getBytes();
            String token = UUID.randomUUID().toString();

            this.companyService.saveWithToken(id, name, phone, email, companyDescription, websiteUrl, memorandumPackage, logoimg, bannerimg, false, token);
            return "redirect:/company/verification-email-sent";
        } catch (Exception e) {
            model.addAttribute("error", true);
            return "companies/registerCompany";
        }
    }

    @GetMapping("/company/verify")
    public String verifyCompany(@RequestParam String token, Model model) {
        Company company = companyService.findByToken(token);
        if (company != null) {
            model.addAttribute("token", token);
            model.addAttribute("verify", "verify");
            return "companies/changePassword";
        } else {
            return "error";
        }
    }

    @PostMapping("/company/set-password")
    public String setPassword(@RequestParam String token, @RequestParam String password, @RequestParam String repeatPassword, Model model) {
        if (!password.equals(repeatPassword)) {
            model.addAttribute("token", token);
            model.addAttribute("error", "Лозинката во двете полиња треба да е иста!");
            return "companies/changePassword";
        }

        Company company = companyService.findByToken(token);
        if (company != null) {
            company.setPassword(passwordEncoder.encode(password));
            company.setActive(true);
            company.setToken(null);
            companyService.save(company);

            String subject = "Password Set Successfully";
            String message = "Your password has been successfully set. Thank you for securing your account.";
            emailService.sendEmail(company.email, subject, message);

            return "redirect:/";
        } else {
            return "error";
        }
    }

    @PostMapping("/company/reset-password")
    public String resetPassword(@RequestParam String name) {
        this.companyService.generateAndSendToken(name);
        return "redirect:/company/verification-email-sent";
    }

    @GetMapping("/company/forgot-password")
    public String showForgotPasswordForm() {
        return "companies/forgotPassword";
    }

    @GetMapping("/company/login")
    public String showCompanyLoginForm() {
        return "companies/companyLogin";
    }

    @GetMapping("/company/verification-email-sent")
    public String showVerificationEmailSent() {
        return "companies/verificationEmailSent";
    }

}