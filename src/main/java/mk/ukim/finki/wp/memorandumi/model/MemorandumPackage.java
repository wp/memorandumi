package mk.ukim.finki.wp.memorandumi.model;

public enum MemorandumPackage {
    BASIC,
    SILVER,
    GOLD,
    SUPPORTED_BY
}
