package mk.ukim.finki.wp.memorandumi.service.interfaces;

import mk.ukim.finki.wp.memorandumi.model.Room;

import java.util.List;

public interface RoomService {
    List<Room> listAllRooms();
}
