package mk.ukim.finki.wp.memorandumi.service.interfaces;

import mk.ukim.finki.wp.memorandumi.model.CompanySubscription;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import mk.ukim.finki.wp.memorandumi.model.MemorandumStatus;
import mk.ukim.finki.wp.memorandumi.model.Room;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface CompanySubscriptionService {
    List<CompanySubscription> listAllCompanySubscriptions();
    CompanySubscription findById(Long id);
    CompanySubscription create(String  companyId, Short year, MemorandumPackage memorandumPackage, MemorandumStatus status, String brandedRoomId, String note);
    CompanySubscription update(Long id, String  companyId, Short year, MemorandumPackage memorandumPackage, MemorandumStatus status, String brandedRoomId, String note);
    CompanySubscription delete(Long id);
    CompanySubscription uploadContract(Long id, byte[] contract);
    CompanySubscription removeContract(Long id);
    Page<CompanySubscription> list(Specification<CompanySubscription> spec, int page, int size);

}
