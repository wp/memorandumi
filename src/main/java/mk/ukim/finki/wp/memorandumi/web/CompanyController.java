package mk.ukim.finki.wp.memorandumi.web;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.MemorandumPackage;
import mk.ukim.finki.wp.memorandumi.service.interfaces.CompanyService;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;
import java.util.List;
import static mk.ukim.finki.wp.memorandumi.service.specifications.FieldFilterSpecification.*;

@Controller
@RequestMapping("/admin/companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public String getAllCompanies(Model model, @RequestParam(required = false) String companyId,
                                  @RequestParam(required = false)MemorandumPackage memorandumPackage,
                                  @RequestParam(required = false) Boolean active,
                                  @RequestParam(defaultValue = "1") Integer pageNum,
                                  @RequestParam(defaultValue = "20") Integer results){

        Specification<Company> spec = Specification.where(filterContainsText(Company.class, "id", companyId));

        spec = spec.and(filterEqualsV(Company.class, "active", active))
                .and(filterEqualsV(Company.class, "memorandumPackage", memorandumPackage));

        Page<Company> companyPage = companyService.list(spec, pageNum, results);

        List<Company> companies = companyService.getAllCompanies();
        model.addAttribute("companyIdSelected",companyId);
        model.addAttribute("memorandumPackageSelected", memorandumPackage);
        model.addAttribute("activeSelected", active);

        model.addAttribute("memorandumPackages", MemorandumPackage.values());
        model.addAttribute("page", companyPage);
        model.addAttribute("companies", companies);

        return "companies/companies";
    }

    @GetMapping("/add")
    public String showAddCompany(Model model){
        model.addAttribute("memorandumPackages", MemorandumPackage.values());
        return "companies/formCompany";
    }

    @PostMapping
    public String addCompany(@RequestParam String id,
                             @RequestParam String name,
                         @RequestParam String phone,
                         @RequestParam String email,
                         @RequestParam String companyDescription,
                         @RequestParam String websiteUrl,
                         @RequestParam MemorandumPackage memorandumPackage,
                         @RequestParam MultipartFile logoImage,
                         @RequestParam MultipartFile banner,
                         @RequestParam Boolean active,
                         Model model) {
        try {
            byte[] logoimg = logoImage.getBytes();
            byte[] bannerimg = banner.getBytes();
            this.companyService.save(id, name, phone, email, companyDescription,websiteUrl,memorandumPackage,logoimg,bannerimg,active);
            return "redirect:/admin/companies";
        } catch (Exception e){
            model.addAttribute("error", true);
            return "companies/formCompany";
        }
    }

    @GetMapping("/edit/{id}")
    public String showEdit(@PathVariable String id, Model model) {
        Company company = this.companyService.findById(id);
        model.addAttribute("memorandumPackages", MemorandumPackage.values());
        model.addAttribute("company",this.companyService.findById(id));
        String logoimageData = Base64.getEncoder().encodeToString(company.getLogoImage());
        model.addAttribute("logoimageData", logoimageData);
        String bannerimageData =  Base64.getEncoder().encodeToString(company.getBanner());
        model.addAttribute("bannerimageData", bannerimageData);
        return "companies/formCompany";
    }

    @PostMapping("/{id}")
    public String edit(@PathVariable String id,
                       @RequestParam String name,
                       @RequestParam String phone,
                       @RequestParam String email,
                       @RequestParam String companyDescription,
                       @RequestParam String websiteUrl,
                       @RequestParam MemorandumPackage memorandumPackage,
                       @RequestParam (required = false) MultipartFile logoImage,
                       @RequestParam (required = false) MultipartFile banner, @RequestParam Boolean active,Model model) {
        try {
            Company existingCompany = this.companyService.findById(id);
            byte[] logoimg = existingCompany.getLogoImage();
            byte[] bannerimg = existingCompany.getBanner();
            if (logoImage != null && !logoImage.isEmpty()) {
                logoimg = logoImage.getBytes();
            }
            if(banner != null && !banner.isEmpty()){
                bannerimg = banner.getBytes();
            }
            this.companyService.edit(id,name, phone, email, companyDescription,websiteUrl,memorandumPackage,logoimg,bannerimg,active);
            return "redirect:/admin/companies";
        } catch (Exception e){
            model.addAttribute("error", true);
            return "companies/formCompany";
        }

    }

    @PostMapping("/delete/{id}")
    public String delete(@PathVariable String id) {
        this.companyService.delete(id);
        return "redirect:/admin/companies";
    }

    @GetMapping("/details/{id}")
    public String showDetails(@PathVariable String id, Model model) {
        Company company = this.companyService.findById(id);
        model.addAttribute("company", company);

        String logoimageData = Base64.getEncoder().encodeToString(company.getLogoImage());
        model.addAttribute("logoimageData", logoimageData);

        String bannerimageData = Base64.getEncoder().encodeToString(company.getBanner());
        model.addAttribute("bannerimageData",bannerimageData);

        return "companies/detailsCompany";
    }


}
