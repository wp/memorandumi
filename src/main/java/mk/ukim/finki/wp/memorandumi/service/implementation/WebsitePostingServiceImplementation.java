package mk.ukim.finki.wp.memorandumi.service.implementation;

import mk.ukim.finki.wp.memorandumi.model.Company;
import mk.ukim.finki.wp.memorandumi.model.WebsitePosting;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidCompanyException;
import mk.ukim.finki.wp.memorandumi.model.exceptions.InvalidWebsitePostingException;
import mk.ukim.finki.wp.memorandumi.repository.CompanyRepository;
import mk.ukim.finki.wp.memorandumi.repository.WebsitePostingRepository;
import mk.ukim.finki.wp.memorandumi.service.interfaces.WebsitePostingService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WebsitePostingServiceImplementation implements WebsitePostingService {
    private final WebsitePostingRepository websitePostingRepository;

    private final CompanyRepository companyRepository;

    public WebsitePostingServiceImplementation(WebsitePostingRepository websitePostingRepository, CompanyRepository companyRepository) {
        this.websitePostingRepository = websitePostingRepository;
        this.companyRepository = companyRepository;
    }


    @Override
    public List<WebsitePosting> getAllWebsitePostings() {
        return websitePostingRepository.findAll();
    }

    @Override
    public WebsitePosting findById(Long id) {
        return websitePostingRepository.findById(id).orElseThrow(InvalidWebsitePostingException::new);
    }

    @Override
    public void save(String companyId, String title, String body, byte[] image) {
        Company company = companyRepository.findById(companyId).orElseThrow(InvalidCompanyException::new);
        WebsitePosting websitePosting = new WebsitePosting(company, title, body, image);
        websitePostingRepository.save(websitePosting);
    }

    @Override
    public void edit(Long id, String companyId, String title, String body, byte[] image) {
        WebsitePosting websitePosting = websitePostingRepository.findById(id).orElseThrow(InvalidWebsitePostingException::new);
        Company company = companyRepository.findById(companyId).orElseThrow(InvalidCompanyException::new);
        websitePosting.setCompany(company);
        websitePosting.setTitle(title);
        websitePosting.setBody(body);
        websitePosting.setImage(image);
        websitePostingRepository.save(websitePosting);
    }

    @Override
    public void delete(Long websitePostingId) {
        WebsitePosting websitePosting = websitePostingRepository.findById(websitePostingId).orElseThrow(InvalidWebsitePostingException::new);
        websitePostingRepository.delete(websitePosting);
    }

    @Override
    public Page<WebsitePosting> list(Specification<WebsitePosting> spec, int page, int size) {
        return websitePostingRepository.findAll(spec, PageRequest.of(page - 1, size));
    }
}


